# Kubernetes
#### Create a Kubernetes cluster using any method you'd like (e.g. kops, minikube, microk8s or Cloud)

Create minikube Kubernetes cluster using following commands:
```bash
brew update
brew install minikube
```

Start minikube cluster with hyperkit driver:

```bash
minikube start --vm-driver=hyperkit
```

## Create a new namespace called staging

```bash
kubectl create namespace staging
```



# Single Pod
#### Create a new pod called hello in this staging namespace, make sure it is in a healthy state. Use the hello image from the step above for this task.


```bash 
kubectl apply -f https://gitlab.com/qamarba/9spokes/-/blob/master/hello.yaml
```




# Multi-node
#### Assume you are operating a 3 node (node-1, node-2, and node-3) cluster, deploy a pod called p2 in every node of your cluster.


DaemonSet is used for multi-node pod implementaton.

```bash
kubeclt apply -f https://gitlab.com/qamarba/9spokes/-/blob/master/multi-node.yaml
```




# Multi-container Pod
#### Create a new pod called p3 in the staging namespace. This pod contains two containers and the pod only should be created when one of these two containers has a file called /app/ready.txt. The pod should fail if that container does not have that file.


InitContainer is used to make sure that the required setup is completed before container ready in a pod for multi-container pods.
```bash
kubectl apply -f https://gitlab.com/qamarba/9spokes/-/blob/master/initContainer.yaml
```




# Monitoring
#### Deploy prometheus alert manager and grafana into the kube-system namespace and configure customised alerts called 9spokes-cpu-alert which is only going to be trigger when a pod has 80% of the CPU running for 60 seconds. You should send the details of this alert with your name into Microsoft Teams Channel located here.


**Install helm on MAC**
```bash
brew install helm
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install prometheus prometheus-community/kube-prometheus-stack

kubectl get all  # to see all pods install by helm
```

**Setup ingress / port-forward**
```bash
kubectl get pods 
```
`prometheus-grafana-7c44f6b8cc-fcpq2`

**Get port:3000 & user: admin from logs of grafana pod**
```bash
kubectl logs prometheus-grafana-7c44f6b8cc-fcpq2  -c grafana    
```

**port-forwarding to expose gafana port 3000 to web**
```bash
kubectl port-forward deployment/prometheus-grafana 3000
```


**port-forwarding prometheus pod**
```bash
kubectl get pod
```                                                                
```
NAME                                                     READY   STATUS    RESTARTS   AGE
alertmanager-prometheus-kube-prometheus-alertmanager-0   2/2     Running   0          29m
prometheus-grafana-7c44f6b8cc-fcpq2                      2/2     Running   0          29m
prometheus-kube-prometheus-operator-f656f9596-jn89r      1/1     Running   0          29m
prometheus-kube-state-metrics-66fb9d645d-gswd5           1/1     Running   0          29m
**prometheus-prometheus-kube-prometheus-prometheus-0       2/2     Running   1          29m**
prometheus-prometheus-node-exporter-84tlz                1/1     Running   0          29m
```

```bash
kubectl port-forward prometheus-prometheus-kube-prometheus-prometheus-0  9090
```


#### Deploy an ingress controller to protect prometheus and alert manager using any authentication method you'd like.

```bash
htpasswd -c auth admin       # admin this the user name

New password: password       # enter your prefered passowrd
Re-type new password: passowrd         # re-enter your prefered passowrd

Adding password for user admin

```

**Create secret file for ingress controller auth**

```bash
kubectl create secret generic basic-auth --from-file=auth
secret "basic-auth" created
```

Below command describe the basic-auth sercert file 
```bash
kubectl get secret basic-auth -o yaml
```

Following annotations must be added in the ingress controller along with prometheus & alert manager urls

```bash
  annotations:
    # type of authentication
    nginx.ingress.kubernetes.io/auth-type: basic
    # name of the secret that contains the user/password definitions
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    # message to display with an appropriate context why the authentication is required
    nginx.ingress.kubernetes.io/auth-realm: 'Authentication Required - foo'
```


**Testing:**

```bash
curl -v http://192.168.64.2/prometheus/graph -H 'Host: 192.168.64.2'


*   Trying 192.168.64.2...
* TCP_NODELAY set
* Connected to 192.168.64.2 (192.168.64.2) port 80 (#0)
> GET /prometheus/graph HTTP/1.1
> Host: 192.168.64.2
> User-Agent: curl/7.64.1
> Accept: */*
> 
< HTTP/1.1 401 Unauthorized
< Date: Mon, 22 Feb 2021 10:59:15 GMT
< Content-Type: text/html
< Content-Length: 172
< Connection: keep-alive
< WWW-Authenticate: Basic realm="Authentication Required - admin"
< 
<html>
<head><title>401 Authorization Required</title></head>
<body>
<center><h1>401 Authorization Required</h1></center>
<hr><center>nginx</center>
</body>
</html>
* Connection #0 to host 192.168.64.2 left intact
* Closing connection 0
```

**User id & password**

```bash
curl -v http://192.168.64.2/prometheus/graph -H 'Host: 192.168.64.2' -u 'admin:pass'



*   Trying 192.168.64.2...
* TCP_NODELAY set
* Connected to 192.168.64.2 (192.168.64.2) port 80 (#0)
* Server auth using Basic with user 'admin'
> GET /prometheus/graph HTTP/1.1
> Host: 192.168.64.2
> Authorization: Basic YWRtaW46cGFzcw==
> User-Agent: curl/7.64.1
> Accept: */*
> 
< HTTP/1.1 200 OK
< Date: Mon, 22 Feb 2021 10:59:38 GMT
< Content-Type: text/html; charset=utf-8
< Transfer-Encoding: chunked
< Connection: keep-alive

```
